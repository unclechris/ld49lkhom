using UnityEngine.Audio;
using UnityEngine;
using UnityEngine.UI;
using System;

public class AudioManager : MonoBehaviour
{

	public static AudioManager Instance;
	[Range(0.0f, 1.0f)]
	public float overAllVolume = 1.0f;
	[Range(0.0f, 1.0f)]
	public float musicVolume = 1.0f;

	public Sound[] Sounds;
	public Sound[] IdleSounds;
	float prevIdleSound;
	string prevIdleName;

	void Awake()
	{
		if (Instance == null)
			Instance = this;
		else
		{
			Destroy(gameObject);
			return;
		}
		DontDestroyOnLoad(gameObject);
		InitSoundArray();
		prevIdleSound = Time.time;
	}
	private void InitSoundArray()
	{
		foreach (Sound s in Sounds)
		{
			s.audioSource = gameObject.AddComponent<AudioSource>();
			s.audioSource.clip = s.clip;
			s.audioSource.volume = s.volume;
			s.audioSource.pitch = s.pitch;
			s.audioSource.loop = s.loop;
			s.audioSource.playOnAwake = s.playOnAwake;
		}

		foreach (Sound s in IdleSounds)
		{
			s.audioSource = gameObject.AddComponent<AudioSource>();
			s.audioSource.clip = s.clip;
			s.audioSource.volume = s.volume;
			s.audioSource.pitch = s.pitch;
			s.audioSource.loop = s.loop;
			s.audioSource.playOnAwake = s.playOnAwake;
		}
	}

	public void PlayIdle()
	{
		if (Time.time - prevIdleSound > 10f)
		{
			if (UnityEngine.Random.Range(1, 100) > 75)
			{
				string playName = prevIdleName;
				while (playName == prevIdleName)
				{
					playName = IdleSounds[UnityEngine.Random.Range(0, IdleSounds.Length)].name;
				}
				Sound s = Array.Find(IdleSounds, sound => sound.name == playName);

				if (s != null)
				{
					if (name == "theme")
					{
						s.audioSource.volume = s.volume * musicVolume;
					}
					else
					{
						s.audioSource.volume = s.volume * overAllVolume;
					}
					s.audioSource.Play();
				}

				prevIdleSound = Time.time+3.0f;
				prevIdleName = playName;
			}
			else
				prevIdleSound = Time.time;
		}
	}
		
	public void Play(string name)
	{
		Sound s = Array.Find(Sounds, sound => sound.name == name);
		if (s != null)
		{
			if (name == "theme")
			{
				s.audioSource.volume = s.volume * musicVolume;
			}
			else
			{
				s.audioSource.volume = s.volume * overAllVolume;
			}
			s.audioSource.Play();
		}
	}

	/*
	public void SetOverallVol(float newValue)
	{
		string samplename = "Jump";
		overAllVolume = newValue;
		if (Sounds != null)
		{
			foreach (Sound s in Sounds)
			{
				if (s != null)
				{
					if (s.name != "theme")
					{
						if (s.audioSource != null)
						{
							s.audioSource.volume = s.volume * overAllVolume;
						}
					}
				}
			}

			Play(samplename);
			PlayerPrefs.SetFloat("OverAllSound", newValue);
		}
	}

	public void SetMusicVol(float newValue)
	{
		musicVolume = newValue;
		if (Sounds != null)
		{
			Sound s = Array.Find(Sounds, sound => sound.name == "theme");
			if (s != null)
			{
				if (s.audioSource != null)
				{
					s.audioSource.volume = s.volume * musicVolume;
				}
			}
			PlayerPrefs.SetFloat("musicSound", newValue);
		}
	}
	*/
	private void Start()
	{
		/*
		if (sliderAllVolume != null)
		{
			sliderAllVolume.value = overAllVolume;
		}
		if (sliderMusicVolume != null)
		{
			sliderMusicVolume.value = musicVolume;
		}
		*/
		Play("Start");
		Play("SavePrincess");
	}

}
