﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LlamaMover : MonoBehaviour
{
    [HideInInspector]
    public bool mustPatrol;
    private bool mustTurn;
    public bool canFlip=true;

    public Rigidbody2D rb;
    public Transform groundCheck;
    public LayerMask groundLayer;
    public float groundCheckSize;
    public Collider2D bodyCollider;
    public LayerMask bodyColliderLayer;
    public LayerMask KrakenLayer;

    public float walkSpeed;

    // Start is called before the first frame update
    void Start()
    {
       mustPatrol = true;
    }

    // Update is called once per frame
    void Update()
    {
       if (mustPatrol)
        {
            Patrol();
        }
        if (bodyCollider.IsTouchingLayers(KrakenLayer))
        {
            HandleSacrifice();
        }
    }

    private void HandleSacrifice()
    {
        Destroy(bodyCollider.gameObject);
    }

    private void FixedUpdate()
    {
        if (mustPatrol)
        {
            mustTurn = !Physics2D.OverlapCircle(groundCheck.position, groundCheckSize, groundLayer);
        }
    }
    private void Patrol()
    {
        if ((canFlip && mustTurn) || bodyCollider.IsTouchingLayers(bodyColliderLayer))
        {
            Flip();
            mustTurn = false;
        }
        Vector2 move = new Vector2(walkSpeed * Time.fixedDeltaTime, rb.velocity.y);
        rb.velocity = move;
    }
    void Flip()
    {
        mustPatrol = false;
        transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
        walkSpeed *= -1;
        mustPatrol = true;
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(groundCheck.position, groundCheckSize);

    }
}
