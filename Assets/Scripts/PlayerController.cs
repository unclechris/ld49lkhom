using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    private PlayerInputActions playerInputs;
    private InputAction movement;
    private void Awake()
    {
        playerInputs = new PlayerInputActions();

    }
    private void OnEnable()
    {
        movement = playerInputs.Player.Move;
        movement.Enable();
        playerInputs.Player.Jump.performed += DoJump;
        playerInputs.Player.Jump.Enable();
    }

    private void OnDisable()
    {
        movement.Disable();
        playerInputs.Player.Jump.performed -= DoJump;
        playerInputs.Player.Jump.Disable();
    }

    private void DoJump(InputAction.CallbackContext obj)
    {
        Debug.Log("Jump!");
    }

    private void FixedUpdate()
    {
        Vector2 move = movement.ReadValue<Vector2>();

        Debug.Log($"movement values {move.x},{move.y}");
    }
}
