using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


public class PlayerMovement : MonoBehaviour
{
    public Rigidbody2D rb;
    public Transform hammerCheck;
    public LayerMask hammerMask;
    public Transform groundCheck;
    public float groundCheckDistance;
    public LayerMask groundLayer;
    public float hammerCheckDistance;

    private float horizontal;
    public float fallMultiplier =2.5f;
    public float lowJumpMultiplier = 2f;
    private float speed = 8f;
    private float jumpingPower = 9.8f * 3f;
    private bool isFacingRight = true;

    private void FixedUpdate()
    {
        rb.velocity = new Vector2(horizontal * speed, rb.velocity.y);
        if (!isFacingRight && horizontal > 0f)
        {
            Flip();
        }
        else if (isFacingRight && horizontal < 0f)
        { 
            Flip();
        }
        if (rb.velocity.y < 0)
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier -1)*Time.deltaTime;
        }
    }
    private bool IsGrounded()
    {
        return Physics2D.OverlapCircle(groundCheck.position, groundCheckDistance, groundLayer);
    }
    private void Flip()
    {
        isFacingRight = !isFacingRight;
        Vector3 localScale = transform.localScale;
        localScale.x *= -1f;
        transform.localScale = localScale;
        
    }
    public void Jump(InputAction.CallbackContext context)
    {
        if (context.performed && IsGrounded())
        {
            rb.velocity = Vector2.up* jumpingPower;
        }

        if (context.canceled && rb.velocity.y > 0f)
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
        }       

    }
    public void Move(InputAction.CallbackContext context)
    {
        horizontal = context.ReadValue<Vector2>().x;
    }
    public void Smash(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            Collider2D collider = Physics2D.OverlapCircle(hammerCheck.position, hammerCheckDistance, hammerMask); 
            if (collider!=null)
            {
                Debug.Log("Smash!");
                Destroy(collider.gameObject);
            }

        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(hammerCheck.position, hammerCheckDistance);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(groundCheck.position, groundCheckDistance);

    }

}
