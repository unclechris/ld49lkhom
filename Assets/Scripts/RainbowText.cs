using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RainbowText : MonoBehaviour
{
	Text txt;
	public float rate;
	float lastTime;
	// Use this for initialization
	void Start()
	{
		txt = GetComponent<Text>();
		lastTime = Time.time;
	}

	// Update is called once per frame
	void Update()
	{
		if ((Time.time - lastTime) > rate)
		{
			Color srColor = txt.color;
			float hue, sat, value;
			Color.RGBToHSV(srColor, out hue, out sat, out value);
			hue += .1f;
			if (hue > 1.0f) { hue -= 1.0f; }
			txt.color = Color.HSVToRGB(hue, sat, value);
			lastTime = Time.time;
		}
	}
}
