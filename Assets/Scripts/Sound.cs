using UnityEngine;
using UnityEngine.Audio;
using System;

[Serializable]
public class Sound
{

	public string name;
	public AudioClip clip;
	[Range(0, 1)]
	public float volume = .78f;
	[Range(.1f, 3)]
	public float pitch = 1f;

	[HideInInspector]
	public AudioSource audioSource;
	public bool loop;
	public bool playOnAwake = false;
}
