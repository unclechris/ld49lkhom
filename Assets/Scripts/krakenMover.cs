using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class krakenMover : MonoBehaviour
{
    public float minY = 0f;
    public float maxY = 1f;
    public float minX = -1.5f;
    public float maxX = 1.5f;
    public float speed = 1.0f;
    public float speedMin = 1.0f;
    public float speedMax = 1.0f;
    public Vector3 pos;
    public Transform myTransform;
    
    void Start()
    {
        ChangePosition();
    }

    private void ChangePosition()
    {
        myTransform.position = new Vector3(UnityEngine.Random.Range(minX, maxX), minY, myTransform.position.z);
        speed = UnityEngine.Random.Range(speedMin, speedMax);
    }

    // Update is called once per frame
    void Update()
    {
        pos = new Vector3(myTransform.position.x, myTransform.position.y + speed * Time.deltaTime, myTransform.position.z);

        //if (pos.y >= maxY ) 
        //{ 
        //    speed *= -1;
        //    pos.y = maxY - .01f;
        //}
        //if (minY - pos.y < .01f && speed < 0.00f) 
        //{ 
        //    ChangePosition(); 
        //}
        myTransform.position = pos;

    }

}
